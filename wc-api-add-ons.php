<?php
/*
 * Plugin Name: Woocommerce api add-ons plugin
 * Description: Add-ons for Woocommerce API
 * Version: 1.0.0
 * Author: Jupser
 * License: GPLv2 or later
 */
 
 require_once'Class/Wc-api-add-ons.class.php';
 $wc_api_add_ons = new Wc_api_add_ons;

 //Settings for adminpanel
 add_action( 'admin_menu',  [$wc_api_add_ons, 'register_section_for_sectings']  );
 add_action( 'admin_init',  [$wc_api_add_ons, 'register_plugin_settings']  );

//Cron
add_filter( 'cron_schedules', [$wc_api_add_ons,'cron_add_new_time_for_wc_add_ons_plugin'] );
if( !wp_next_scheduled('wc_add_ons_plugin_hook') ){ wp_schedule_event( time(), 'custom_cron_time_for_wc_add_ons_plugin', 'wc_add_ons_plugin_hook'); }
//add_action( 'after_setup_theme',  [$wc_api_add_ons,'send_not_answered_orders_request'] );
add_action( 'wc_add_ons_plugin_hook', [$wc_api_add_ons,'send_not_answered_orders_request']  );

//If order was created
add_action( 'woocommerce_new_order', [$wc_api_add_ons, 'send_order_create_request']);
	 
//If order was changed
add_action( 'woocommerce_update_order', [$wc_api_add_ons, 'send_order_update_request']);


//If note was added
add_action( 'wp_insert_comment', [$wc_api_add_ons, 'send_notes_update_request'], 10, 2);

add_action( 'deleted_comment', [$wc_api_add_ons, 'send_notes_update_request'], 10, 2);

// Add custom data to API response
add_filter( 'woocommerce_rest_prepare_shop_order_object', [$wc_api_add_ons, 'add_custom_data_to_api_response'], 10, 3 );
	  
//If order was deleted
add_action('before_delete_post', function($id) 
{	$wc_api_add_ons = new Wc_api_add_ons;
    global $post_type; 

    if($post_type !== 'shop_order') {
        return;
    }
 
   $wc_api_add_ons->send_order_delete_request($id);
}, 10, 1);
