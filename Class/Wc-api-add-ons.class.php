<?php
Class Wc_api_add_ons
{   protected $order_update_url = null;
    protected $order_create_url = null;
    protected $order_delete_url = null;
    protected $correct_server_response = null;
    protected $plugin_section_name =  'wc_api_add_ons_setups';
    protected $plugin_section_title = 'Printflow';


    public function __construct()
    {
        $this->order_update_url = get_option('order_update_url').'?site='.(empty(get_option('sitename'))  ?  $_SERVER['HTTP_HOST'] : get_option('sitename')).'&api_key='.get_option('api_key');
        $this->order_create_url = get_option('order_create_url').'?site='.(empty(get_option('sitename'))  ?  $_SERVER['HTTP_HOST'] : get_option('sitename')).'&api_key='.get_option('api_key');
        $this->order_delete_url = get_option('order_delete_url').'?site='.(empty(get_option('sitename'))  ?  $_SERVER['HTTP_HOST'] : get_option('sitename')).'&api_key='.get_option('api_key');
        $this->order_cron_query_url = get_option('order_cron_query_url');
        $this->auth_token = get_option('auth_token');
        $this->scheme = isset($_SERVER['HTTP_SCHEME']) ? $_SERVER['HTTP_SCHEME'] : (((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || 443 == $_SERVER['SERVER_PORT']) ? 'https://' : 'http://');
    }

    private function add_customer($order_data = null)

    {   //if(!class_exists(Uni_Wc_Orders_App)) return;

        if(!isset($order_data['customer_id'])) return $order_data;

        if($order_data['customer_id'] < 1) return $order_data;

        $customer = get_user_by('id', $order_data['customer_id']);

        $order_data['customer'] = [
            'last_name' => get_user_meta( $customer->data->ID, 'last_name', true ),
            'first_name' => get_user_meta( $customer->data->ID, 'first_name', true ),
            'company' => get_user_meta( $customer->data->ID, 'shipping_company', true ),
            'email' => $customer->data->user_email,
            'id' => $customer->data->ID
        ];

        return $order_data;
    }

    private function add_pdf($order_data = null)
    {    //if(!class_exists('Uni_Wc_Orders_App')) return;

        if(!is_array($order_data)) return $order_data;

        // $order_data['pdf'] =
        //     [
        //         'invoice-order'          => $this->scheme.$_SERVER['HTTP_HOST'].'/wp-content/uploads/wc-orders-app-pdf/invoice-order-'.$order_data['number'].'.pdf',
        //         'packing-slip-order'     => $this->scheme.$_SERVER['HTTP_HOST'].'/wp-content/uploads/wc-orders-app-pdf/packing-slip-order-'.$order_data['number'].'.pdf',
        //         'price-quote-order'      => $this->scheme.$_SERVER['HTTP_HOST'].'/wp-content/uploads/wc-orders-app-pdf/price-quote-order-'.$order_data['number'].'.pdf',
        //         'proforma-invoice-order' => $this->scheme.$_SERVER['HTTP_HOST'].'/wp-content/uploads/wc-orders-app-pdf/proforma-invoice-order-'.$order_data['number'].'.pdf',
        //     ];
        $order_data['pdf'] = [
            'invoice-order'  => $this->scheme.$_SERVER['HTTP_HOST'].'/wp-admin/admin-ajax.php?action=generate_uni_wc_pdf&document_type=invoice&order_id=' . $order_data['id'],
            'packing-slip-order' => $this->scheme.$_SERVER['HTTP_HOST'].'/wp-admin/admin-ajax.php?action=generate_uni_wc_pdf&document_type=packing-slip&order_id=' . $order_data['id'],
            'price-quote-order' => $this->scheme.$_SERVER['HTTP_HOST'].'/wp-admin/admin-ajax.php?action=generate_uni_wc_pdf&document_type=price-quote&order_id=' . $order_data['id'],
            'proforma-invoice-order' => $this->scheme.$_SERVER['HTTP_HOST'].'/wp-admin/admin-ajax.php?action=generate_uni_wc_pdf&document_type=proforma&order_id=' . $order_data['id']
        ];

        return $order_data;
    }

    private function add_order_notes($order_data = null) {

        if (!is_array($order_data)) return $order_data;

        remove_filter('comments_clauses', array('WC_Comments', 'exclude_order_comments'));

        $comments = get_comments(array(
            'post_id' => $order_data['id'],
            'orderby' => 'comment_ID',
            'order'   => 'DESC',
            'approve' => 'approve',
            'type'    => 'order_note',
        ) );

        add_filter( 'comments_clauses', array( 'WC_Comments', 'exclude_order_comments' ) );

        $order_data['notes'] = $comments;

        return $order_data;
    }

    public function send_not_answered_orders_request()
    {   add_action( 'woocommerce_after_register_post_type', [$this, 'orders_prepare_for_send_request']);
    }

    public function orders_prepare_for_send_request()
    {   $result = [];
        $GLOBALS['run_meta_filter_on_query'] = false;
        $orders = get_posts
        (
            [   'orderby'     => 'date',
                'order'       => 'DESC',
                'numberposts' => - 1,
                'meta_key'    => ['transfered' => 0],
                'post_type'   => ['shop_order'],
                'post_status' => ['wc-export-error','wc-shipping','wc-back-order','wc-credit-release', 'wc-completed','wc-pending', 'wc-on-hold', 'wc-cancelled', 'wc-failed', 'wc-credit-hold']
            ]
        );
        $GLOBALS['run_meta_filter_on_query'] = true;
        wp_reset_postdata();

        foreach($orders as $key => $item)
        {
            $order = wc_get_order( $item->ID );
            if($order->get_meta('transfered') == 0)
            {   $order_data = $order->get_data();
                $order_data = $this->add_pdf($order_data);
                $order_data = $this->add_customer($order_data);
                $result[] = $order_data;
            }
        }

        $this->send_not_transacted_orders($result);
    }

    public function send_order_delete_request($order_id = null)
    {   if( !isset($order_id) ) return false;

        $order_data = json_encode(['id' => $order_id]);

        $url = str_replace('{order_id}',$order_id,$this->order_delete_url);

        if($this->curl_request($order_data, $url,'DELETE') == $this->correct_server_response)
        {
            return true;
        }

        return false;
    }

    public function send_order_create_request($order_id = null)
    {   if( !isset($order_id) )   return false;

        $order_data = $this->get_order_by_id($order_id);

        $order_data = $this->add_pdf($order_data);

        $order_data = $this->add_customer($order_data);

        $order_data = $this->add_order_notes($order_data);

        $order_data = json_encode($order_data);

        if($this->curl_request($order_data, $this->order_create_url) == $this->correct_server_response)
        {
            if($this->order_update_by_id($order_id, 1)) return true;
        }

        $this->order_update_by_id($order_id, 0);

        return false;
    }

    public function add_custom_data_to_api_response($response, $object, $request) {

        if (empty($response->data)) {
            return $response;
        }

        $response->data = $this->add_pdf($response->data);
        $response->data = $this->add_order_notes($response->data);
        $response->data = $this->add_customer($response->data);

        return $response;
    }

    public function send_not_transacted_orders($data = [])
    {   $data_json = json_encode($data);

        if($this->curl_request($data_json, $this->order_cron_query_url) == $this->correct_server_response)
        {
            foreach($data as $key => $item)
            {   $this->order_update_by_id($item['id'], 1);
            }
        }

        return false;
    }

    public function send_order_update_request($order_id = null)
    {
        if( !isset($order_id) ) return false;

        $url = str_replace('{order_id}',$order_id,$this->order_update_url);

        $order_data = $this->get_order_by_id($order_id);

        $order_data = $this->add_pdf($order_data);

        $order_data = $this->add_customer($order_data);

        $order_data = $this->add_order_notes($order_data);

        $order_data = json_encode($order_data);

        if($this->curl_request($order_data, $url, 'PUT') == $this->correct_server_response)
        {
            if($this->order_update_by_id($order_id, 1)) return true;
        }

        $this->order_update_by_id($order_id, 0);

        return false;
    }

    public function send_notes_update_request($id, $comment)
    {
        $order_id = $comment->comment_post_ID;
        $url = str_replace('{order_id}', $order_id, $this->order_update_url);

        $order_data = $this->get_order_by_id($order_id);

        if (!$order_data) return;

        $order_data = $this->add_pdf($order_data);

        $order_data = $this->add_customer($order_data);

        $order_data = $this->add_order_notes($order_data);

        $order_data['notes'][] = (object) $note;

        $order_data = json_encode($order_data);

        if($this->curl_request($order_data, $url, 'PUT') == $this->correct_server_response)
        {
            if($this->order_update_by_id($order_id, 1)) return true;
        }

        $this->order_update_by_id($order_id, 0);
    }



    private function order_update_by_id($order_id = null, $transfered = null)
    {
        if( !isset($order_id) or !isset($transfered) ) return false;

        $order = wc_get_order( $order_id );

        if(empty($order->get_meta('transfered')))
        {   $order->add_meta_data( 'transfered',$transfered, true);
        }else{
            $order->update_meta_data( 'transfered',  $transfered);
        }

        remove_action( 'woocommerce_update_order', [$this, 'send_order_update_request']);

        $order->save();

        return true;
    }

    private function get_order_by_id($order_id = null)
    {   if( !isset($order_id) ) return false;

        $order = wc_get_order( $order_id );

        if (!$order) return false;

        $line_items = [];
        foreach ($order->get_items() as $item_id => $item_data) {
            $line_items[] =
            [   'id' =>  $item_id,
                'name' =>  $item_data['name'],
                'quantity' => $item_data['quantity'],
                'total' => $item_data['total'],
                'meta_data' => $item_data->get_meta_data()
            ];
        }
        $order = $order->get_data();

        $order['line_items'] = $line_items;

        return $order;
    }

    private function curl_request($data = null, $url = null, $method = 'POST')
    {   if( !isset($url) or !isset($data) )  return false;

        $ch = curl_init();
        if(!empty($this->auth_token)) curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        $result = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close ($ch);
        
        return $http_code;
    }

    public function register_section_for_sectings()
    {
        add_menu_page( $this->plugin_section_title, $this->plugin_section_title, 'manage_options', $this->plugin_section_name, [$this, 'render_wc_api_add_ons_setups'] );
    }

    public function render_wc_api_add_ons_setups()
    {
        echo'<form action="options.php" method="POST">';
        do_settings_sections( $this->plugin_section_name);
        settings_fields( $this->plugin_section_name );
        submit_button();
        echo'</form>';
    }

    public function register_plugin_settings()
    {
        $optionsArr = [
            'sitename' =>
                [   'type' => 'textarea',
                    'name' => 'Site name:',
                    'callback' => 'render_field',
                    'section_name' => $this->plugin_section_name,
                ],
            'api_key' =>
                [   'type' => 'textarea',
                    'name' => 'API key:',
                    'callback' => 'render_field',
                    'section_name' => $this->plugin_section_name,
                ],
            'order_create_url' =>
                [   'type' => 'textarea',
                    'name' => 'Postback URL for "Order create":',
                    'callback' => 'render_field',
                    'section_name' => $this->plugin_section_name,
                ],
            'order_update_url' =>
                [   'type' => 'textarea',
                    'name' => 'Postback URL for "Order update". <i>Avalible macroses: {order_id} - order ID</i>:',
                    'callback' => 'render_field',
                    'section_name' => $this->plugin_section_name,
                ],
            'order_delete_url' =>
                [   'type' => 'textarea',
                    'name' => 'Postback URL for "Order delete". <i>Avalible macroses: {order_id} - order ID</i>:',
                    'callback' => 'render_field',
                    'section_name' => $this->plugin_section_name,
                ],
            'order_cron_query_url' =>
                [   'type' => 'textarea',
                    'name' => 'URL for cron request with not correct response from your server:',
                    'callback' => 'render_field',
                    'section_name' => $this->plugin_section_name,
                ],
            'correct_server_response' =>
                [   'type' => 'textarea',
                    'name' => 'Response header code of server for "Success":',
                    'callback' => 'render_field',
                    'section_name' => $this->plugin_section_name,
                ],
            'cron_interval' =>
                [   'type' => 'text',
                    'name' => 'Cron interval for auto postback (will used if server don\'t unsver when this was need) (Hours):',
                    'callback' => 'render_field',
                    'section_name' => $this->plugin_section_name,
                ],
        ];
        add_settings_section($this->plugin_section_name, $this->plugin_section_title, '', $this->plugin_section_name);

        $this->create_field($optionsArr);
    }

    private function create_field($optionsArr = [])
    {
        foreach($optionsArr as $key=>$item)
        {
            register_setting($item['section_name'],$key);
            add_settings_field(
                $key,
                $item['name'],
                [$this, $item['callback']],
                $item['section_name'],
                $item['section_name'],
                [   'type' => $item['type'],
                    'id' => $key,
                    'option_name' => $key
                ]
            );
        }
    }

    public function render_field( $val ){
        switch($val['type'])
        {   case 'text':
            echo'
                <input  
                    type="text"  
                    name="'.$val['option_name'].'" 
                    value="'.esc_attr( get_option($val['option_name']) ).'" 
                />';
            break;
            case 'textarea':
                echo'<textarea   name="'.$val['option_name'].'">'.esc_attr( get_option($val['option_name']) ).'</textarea>';
                break;
        }
    }

    public  function cron_add_new_time_for_wc_add_ons_plugin( $schedules ) {
        $schedules['custom_cron_time_for_wc_add_ons_plugin'] = array(
            'interval' => 60 * get_option( 'cron_interval'),
            'display' => 'custom_cron_time_for_wc_add_ons_plugin'
        );
        return $schedules;
    }
}